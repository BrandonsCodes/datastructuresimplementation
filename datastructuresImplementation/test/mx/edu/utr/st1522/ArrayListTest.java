package mx.edu.utr.st1522;

import org.junit.Test;
import static org.junit.Assert.*;

public class ArrayListTest {
 
    /**
     * Test of add method.
     */
    @Test
    public void testAdd_Object() {
        Object element = new Object();
        ArrayList instance = new ArrayList();
        assertTrue(instance.add(element));
        assertSame(element, instance.get(0));
        assertEquals(1, instance.size());
    }

    /**
     * Test of add method at index.
     */
    @Test
    public void testAdd_int_Object() {
        int index = 0;
        Object element = new Object();;
        ArrayList instance = new ArrayList();
        instance.add(index, element);
        assertSame(element, instance.get(0));
        assertEquals(1, instance.size());
    }

    /**
     * Test that add method enforces the limits of the list.
     */
    @Test(expected = Exception.class)
    public void testAdd_int_Object_OutOfBounds() {
        int index = 1;
        Object element = new Object();
        ArrayList instance = new ArrayList();
        instance.add(index, element);
    }

    /**
     * Test of clear method.
     */
    @Test
    public void testClear() {
        Object element = new Object();
        ArrayList instance = new ArrayList();
        instance.add(element);
        instance.clear();
        assertEquals(0, instance.size());
    }

    /**
     * Test of get method.
     */
    @Test
    public void testGet() {
        int index = 0;
        Object element = new Object();
        ArrayList instance = new ArrayList();
        instance.add(element);
        assertSame(element, instance.get(index));
    }

    /**
     * Test that get method enforces the limits of the list;.
     */
    @Test(expected = Exception.class)
    public void testGet_OutOfBounds() {
        int index = 1;
        ArrayList instance = new ArrayList();
        instance.get(index);
    }

    /**
     * Test of indexOf method for a existing element.
     */
    @Test
    public void testIndexOf_Existent() {
        Object element = new Object();
        ArrayList instance = new ArrayList();
        instance.add(element);
        assertEquals(0, instance.indexOf(element));
    }

    /**
     * Test of indexOf method for an nonexistent element.
     */
    @Test
    public void testIndexOf_Unexistent() {
        Object element = new Object();
        Object nonexistentElement = new Object();
        ArrayList instance = new ArrayList();
        instance.add(element);
        assertEquals(-1, instance.indexOf(nonexistentElement));
    }

    /**
     * Test of isEmpty method.
     */
    @Test
    public void testIsEmpty() {
        ArrayList instance = new ArrayList();
        boolean result = instance.isEmpty();
        assertTrue(result);
    }

    /**
     * Test of remove method.
     */
    @Test
    public void testRemove() {
        int index = 0;
        Object element = new Object();
        ArrayList instance = new ArrayList();
        instance.add(element);
        Object result = instance.remove(index);
        assertSame(element, result);
        assertEquals(0, instance.size());
    }

    /**
     * Test that remove method enforces the limits of the list.
     */
    @Test(expected = Exception.class)
    public void testRemove_OutOfBounds() {
        int index = 1;
        ArrayList instance = new ArrayList();
        instance.remove(index);
    }

    /**
     * Test of set method.
     */
    @Test
    public void testSet() {
        int index = 0;
        Object element = new Object();
        Object newElement = new Object();
        ArrayList instance = new ArrayList();
        instance.add(element);
        Object result = instance.set(index, newElement);
        assertSame(element, result);
        assertSame(newElement, instance.get(index));
    }

    /**
     * Test that set method enforces the limits of the list;.
     */
    @Test(expected = Exception.class)
    public void testSet_OutOfBounds() {
        int index = 0;
        Object element = new Object();;
        ArrayList instance = new ArrayList();
        Object result = instance.set(index, element);
    }

    /**
     * Test of size method
     */
    @Test
    public void testSize() {
        ArrayList instance = new ArrayList();
        assertEquals(0, instance.size());
    }
    
}