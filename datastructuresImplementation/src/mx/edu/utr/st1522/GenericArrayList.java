/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utr.st1522;

import java.util.Arrays;

/**
 * Description This GenericArrayList is used to add, remove, get items from an
 * GenericArrayList these are the methods using own no from a library
 *
 * @author Brandon
 * @param <T>
 */
public class GenericArrayList<T> {

    private T[] elements;

    private int size;

    public GenericArrayList() {
        this(10);
    }

    public GenericArrayList(int initialCapacity) {
        elements = (T[]) new Object[initialCapacity];
    }

    /**
     * {@inheritDoc }
     *
     * @param type
     * @return
     */
    //@Override
    public boolean add(T type) {
        ensureCapacity(size + 1);//Make sure of capacity
        elements[size++] = type;//Given in te position size++ the type(object)
        return true;
    }

    /**
     * {@inheritDoc }
     *
     * @param index
     * @param element
     */
    public void add(int index, T element) {
        rangeCheckForAdd(index);//Check if there are a space in the array where to put the element(object) or if the position is no less of 0
        ensureCapacity(size + 1);
        System.arraycopy(elements, index, elements, index + 1, size - index);
        elements[index] = element;//put in the position index the element(object)
        size++;
    }

    /**
     * {@inheritDoc }
     */
    public void clear() {
        for (int i = 0; i < size; i++) {//starting to fill with null from 0 to the total size
            elements[i] = null;//giving null to all positions
            size = 0;//decreasing to 0 size
        }
    }

    /**
     * {@inheritDoc }
     *
     * @param index
     * @return
     */
    public Object get(int index) {
        outOfBound(index);//Check if the element(object) "index" is in the array "size"
        return elements[index];
    }

    /**
     * {@inheritDoc }
     *
     * @param element
     * @return
     */
    public int indexOf(T element) {
        if (element == null) {
            for (int i = 0; i < size; i++) {
                if (elements[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (element.equals(elements[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     * {@inheritDoc }
     *
     * @return
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * {@inheritDoc }
     *
     * @param index
     * @return
     */
    public Object remove(int index) {
        outOfBound(index);
        Object oldElement = elements[index];

        int numberMoved = size - index - 1;
        if (numberMoved > 0) {
            System.arraycopy(elements, index + 1, elements, index,
                    numberMoved);
        }
        elements[--size] = null;
        return oldElement;
    }

    /**
     * {@inheritDoc }
     *
     * @param index
     * @param element
     * @return
     */
    public Object set(int index, T element) {
        outOfBound(index);
        Object old = elements[index];
        elements[index] = element;
        return old;
    }

    /**
     * {@inheritDoc }
     *
     * @return
     */
    public int size() {
        return size;
    }

    private void ensureCapacity(int minCapacity) {
        int oldCapacity = elements.length;
        if (minCapacity > oldCapacity) {
            //generar nuevo array con newCapacity
            int newCapacity = oldCapacity * 2;
            if (newCapacity < minCapacity) {
                newCapacity = minCapacity;
            }
            //elements apunta al nuevo array

            elements = Arrays.copyOf(elements, newCapacity);

            //copiar info al nuevo array
        }
    }

    //the same pourpose of otOfBound method
    private void rangeCheckForAdd(int index) {
        if (index > size || index < 0) {
            throw new IndexOutOfBoundsException();
        }
    }
    //added rangeCheckForAdd because the teacher want it

    //the same pourpose of rangeCheckForAdd method
    private void outOfBound(int index) {
        if (index >= size) {
            throw new ArrayIndexOutOfBoundsException("Out of Bound");
        }

    }
}
