/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utr.st1522;

import java.util.Iterator;
import java.util.NoSuchElementException;
import mx.edu.utr.datastructures.*;

/**
 *
 * @author Brandon
 * @param <T>
 */
public class LinkedList<T> implements List<T>, Iterable<T>, Queue<T> {

    private Node<T> head;
    private Node<T> tail;
    private int size;

    /**
     * Creates an empty list.
     */
    public LinkedList() {
        head = new Node<>(null, null, null);
        tail = new Node<>(null, head, null);
        head.next = tail;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean add(T element) {
        addBefore(element, tail);
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void add(int index, T element) {
        checkPositionIndex(index);
        if (index == size) {
            add(element);
        } else {
            addBefore(element, getNode(index));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear() {
        head.next = tail;
        tail.previous = head;
        size = 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T get(int index) {
        IndexOutOfBound(index);
        return (T) getNode(index).item;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int indexOf(T element) {
        int index = 0;
        for (Node n = head.next; n != tail; n = n.next) {
            if (element == null) {
                if (n.item == element) {
                    return index;
                }
            } else {
                if (n.item.equals(element)) {
                    return index;
                }
            }
            index++;
        }
        return -1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T remove(int index) {
        checkPositionIndex(index);
        Node n = getNode(index);
        n.previous.next = n.next;
        n.next.previous = n.previous;
        size--;
        return (T) n.item;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T set(int index, T element) {
        checkPositionIndex(index);
        Node n = getNode(index);
        Object old = n.item;
        n.item = element;
        return (T) old;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Adds a new node with element as content before node.
     *
     * @param element
     * @param node
     */
    private void addBefore(T element, Node node) {
        /**
         * **************************************
         */
        Node newNode = new Node(element, node.previous, node);
        node.previous.next = newNode;
        node.previous = newNode;
        size++;
        /**
         * **************************************
         */
    }

    private Node getNode(int index) {
        /**
         * **************************************
         */
        if (index < 0 || index > size - 1) {
            throw new IndexOutOfBoundsException();
        }
        Node n;
        if (index <= size / 2) {
            n = head.next;
            for (int i = 0; i < index; i++) {
                n = n.next;
            }
        } else {
            n = tail.previous;
            for (int i = size - 1; i > index; i--) {
                n = n.previous;
            }
        }
        return n;
        /**
         * **************************************
         */
    }

    /**
     * Node structure for the list implementation.
     */
    private class Node<T> {

        T item;
        Node<T> next;
        Node<T> previous;

        /**
         * Creates a new instance of the node.
         */
        public Node(T item, Node<T> previous, Node<T> next) {
            this.item = item;
            this.previous = previous;
            this.next = next;
        }
    }

    private void IndexOutOfBound(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);

        }
    }

    private void checkPositionIndex(int index) {
        if (!isPositionIndex(index)) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
    }

    private boolean isPositionIndex(int index) {
        return index >= 0 && index <= size;
    }

    private String outOfBoundsMsg(int index) {
        return "Index: " + index + ", Size: " + size;
    }

    @Override
    public Iterator<T> iterator() {
        return new Itr();
    }

    private class Itr implements Iterator<T> {

        Node<T> cursor;

        public Itr() {
            cursor = head;
        }

        @Override
        public boolean hasNext() {
            return cursor.next != tail;
        }

        @Override
        public T next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            T returnValue = cursor.next.item;
            cursor = cursor.next;
            return returnValue;
        }

    }

    @Override
    public boolean push(T element) {
        add(0, element);
        return true;
    }

    @Override
    public T pop() {
        return remove(size - 1);
    }

    @Override
    public T peek() {
        return get(size - 1);
    }
}
