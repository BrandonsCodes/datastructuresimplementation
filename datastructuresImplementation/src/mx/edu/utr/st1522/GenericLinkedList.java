/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utr.st1522;

/**
 *
 * @author Brandon
 */
public class GenericLinkedList<T> {

    private int size = 0;

    Node<T> header = new Node<>(null, null, null);

    public GenericLinkedList() {

        header.next = header.previous = header;

    }

    public boolean add(T type) {
        addBefore(type, header);
        return true;
    }

    public void add(int index, T type) {
        checkPositionIndex(index);
        if (index == size) {
            addBefore(type, header);
        } else {
            addBefore(type, getNode(index));
        }
    }

    public void clear() {
        Node<T> t = header.next;
        while (t != header) {
            Node<T> next = t.next;
            t.next = t.previous = null;
            t.type = null;
            t = next;
        }
        header.next = header.previous = header;
        size = 0;

    }

    public T get(int index) {
        IndexOutOfBound(index);
        return (T) getNode(index).type;
    }

    public int indexOf(T type) {
        int index = 0;
        if (type == null) {
            for (Node t = header.next; t != header; t = t.next) {
                if (t.type == null) {
                    return index;
                }
                index++;
            }
        } else {
            for (Node t = header.next; t != header; t = t.next) {
                if (type.equals(t.type)) {
                    return index;
                }
                index++;
            }
        }
        return -1;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public T remove(int index) {
        checkPositionIndex(index);
        Node<T> t = getNode(index);
        T oldVal = t.type;
        t.previous.next = t.next;
        t.next.previous = t.previous;
        t.next = t.previous = null;
        t.type = null;
        size--;
        return oldVal;
        
    }

    public T set(int index, T type) {
        checkPositionIndex(index);
        Node<T> t = getNode(index);
        T oldVal = t.type;
        t.type = type;
        return oldVal;
    }

    public int size() {
        return size;
    }

    private void addBefore(Object element, Node node) {

        Node newNode = new Node(element, node, node.next);
        newNode.previous.next = newNode;
        newNode.next.previous = newNode;
        size++;

    }

    private Node getNode(int index) {
        Node head = header;
        if (index < (size >> 1)) {
            for (int i = 0; i <= index; i++) {
                head = head.next;
            }
        } else {
            for (int i = size; i > index; i--) {
                head = head.previous;
            }
        }
        return head;
    }

    private void IndexOutOfBound(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index: " + index + ", Size: " + size);

        }
    }

    private void checkPositionIndex(int index) {
        if (!isPositionIndex(index)) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
    }

    private boolean isPositionIndex(int index) {
        return index >= 0 && index <= size;
    }

    private String outOfBoundsMsg(int index) {
        return "Index: " + index + ", Size: " + size;
    }

}

class Node<T> {

    T type;
    Node<T> previous;
    Node<T> next;

    Node(T type, Node<T> previous, Node<T> next) {

        this.type = type;
        this.previous = previous;
        this.next = next;

    }

}
