/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utr.st1522;

import java.util.Iterator;
import java.util.NoSuchElementException;
import mx.edu.utr.datastructures.*;

/**
 * Description This ArrayList is used to add, remove, get items from an
 * ArrayList these are the methods using own no from a library
 *
 * @author Brandon
 * @param <T>
 */
public class ArrayList<T> implements List<T>, Iterable<T>, Queue<T> {

    private T[] elements;

    private int size;

    public ArrayList() {
        this(10);
    }

    public ArrayList(int initialCapacity) {
        elements = (T[]) new Object[initialCapacity];
    }

    /**
     * {@inheritDoc }
     *
     * @param element
     * @return
     */
    @Override
    public boolean add(T element) {
        ensureCapacity(size + 1);
        elements[size++] = element;
        return true;
    }

    /**
     * {@inheritDoc }
     *
     * @param index
     * @param element
     */
    @Override
    public void add(int index, T element) {
        rangeCheckForAdd(index);
        ensureCapacity(size + 1);
        for (int i = size - 1; i >= index; i--) {
            elements[i + 1] = elements[i];
        }
        elements[index] = element;
        size++;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public void clear() {
        for (int i = 0; i < size; i++) {
            elements[i] = null;
            size = 0;
        }
    }

    /**
     * {@inheritDoc }
     *
     * @param index
     * @return
     */
    @Override
    public T get(int index) {
        checkRange(index);
        return elements[index];
    }

    /**
     * {@inheritDoc }
     *
     * @param element
     * @return
     */
    @Override
    public int indexOf(T element) {
        if (element == null) {
            for (int i = 0; i < size; i++) {
                if (elements[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (element.equals(elements[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     * {@inheritDoc }
     *
     * @return
     */
    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * {@inheritDoc }
     *
     * @param index
     * @return
     */
    @Override
    public T remove(int index) {
        checkRange(index);
        T oldElement = elements[index];
        for (int i = index + 1; i < size; i++) {
            elements[i - 1] = elements[i];
            elements[i] = null;
        }
        size--;
        return oldElement;
    }

    /**
     * {@inheritDoc }
     *
     * @param index
     * @param element
     * @return
     */
    @Override
    public T set(int index, T element) {
        checkRange(index);
        T old = elements[index];
        elements[index] = element;
        return old;
    }

    /**
     * {@inheritDoc }
     *
     * @return
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Increases the capacity if necessary.
     *
     * @param minCapacity the desired minimum capacity.
     */
    private void ensureCapacity(int minCapacity) {
        int oldCapacity = elements.length;
        if (minCapacity > oldCapacity) {
            //generar nuevo array con newCapacity
            int newCapacity = oldCapacity * 2;
            if (newCapacity < minCapacity) {
                newCapacity = minCapacity;
            }
            //elements apunta al nuevo array
            T[] currentElements = elements;
            elements = (T[]) new Object[newCapacity];
            //copiar info al nuevo array
            for (int i = 0; i < size; i++) {
                elements[i] = currentElements[i];
            }

        }
    }

    /**
     * Checks if the index is acceptable. Used in add where adding in the index
     * after the index of the last item in the list is acceptable.
     *
     * @param index the index to be evaluated.
     */
    private void rangeCheckForAdd(int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException("Invalid index.");
        }
    }

    /**
     * Checks if the index is acceptable. Only indexes of items in the list are
     * acceptable.
     *
     * @param index the index to be evaluated.
     */
    private void checkRange(int index) {
        if (index < 0 || index > size - 1) {
            throw new IndexOutOfBoundsException("Invalid index.");
        }

    }

    @Override
    public Iterator<T> iterator() {
        return new Itr();
    }

    private class Itr implements Iterator<T> {

        int cursor;

        @Override
        public boolean hasNext() {
            return cursor != size;
        }

        @Override
        public T next() {
            if (cursor >= size) {
                throw new NoSuchElementException();
            }
            return elements[cursor++];
        }
    }
    
    @Override
    public boolean push(T element) {
        add(0, element);
        return true;
    }

    @Override
    public T pop() {
        return remove(size - 1);
    }

    @Override
    public T peek() {
        return get(size - 1);
    }
}
